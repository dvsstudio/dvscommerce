package ru.daniils.dvscommerce.activities;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import ru.daniils.dvscommerce.Core;
import ru.daniils.dvscommerce.R;
import ru.daniils.dvscommerce.Utils;
import ru.daniils.dvscommerce.entities.Landing;
import ru.daniils.dvscommerce.entities.Site;
import ru.daniils.dvscommerce.fragments.GoogleFragment;
import ru.daniils.dvscommerce.fragments.InfoFragment;
import ru.daniils.dvscommerce.fragments.LeadsFragment;
import ru.daniils.dvscommerce.fragments.SettingsFragment;
import ru.daniils.dvscommerce.fragments.StatisticsFragment;
import ru.daniils.dvscommerce.fragments.VKFragment;
import ru.daniils.dvscommerce.fragments.WidgetsFragment;
import ru.daniils.dvscommerce.fragments.YandexFragment;

/*
Главная активность приложения
*/
public class MainActivity extends AppCompatActivity {
    private final static int REQUEST_LOGIN_NEW_SITE = 1;
    private static Fragment curFragment;
    private static InfoFragment infoFragment = null;
    private static LeadsFragment leadsFragment = null;
    private static WidgetsFragment widgetsFragment = null;
    private static StatisticsFragment statisticsFragment = null;
    private static YandexFragment yandexFragment = null;
    private static GoogleFragment googleFragment = null;
    private static VKFragment vkFragment = null;
    private static SettingsFragment settingsFragment = null;

    private Toolbar toolbar;
    private DrawerLayout drawer;
    private NavigationView rightNavMenu;
    private Core core;
    private int rightNavMenuOrder = 0;
    private int rightNavMenuGroup = -1;
    private TextView curLandingTextView;
    private ImageView curLandingFaviconView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = getIntent();

        core = Core.getInstance();
        core.setActivity(this);
        if (!Utils.checkPermission(android.Manifest.permission.CALL_PHONE))
            Utils.requestPermission(android.Manifest.permission.CALL_PHONE);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer == null) {
            return;
        }

        NavigationView leftNavMenu = (NavigationView) findViewById(R.id.left_nav_menu);
        rightNavMenu = (NavigationView) findViewById(R.id.right_nav_menu);
        if (rightNavMenu == null || leftNavMenu == null) {
            return;
        }

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        leftNavMenu.setItemIconTintList(null);
        rightNavMenu.setItemIconTintList(null);

        leftNavMenu.setNavigationItemSelectedListener(new LeftNavigationMenuItemListener());
        rightNavMenu.setNavigationItemSelectedListener(new RightNavigationMenuItemListener());

        if (core.curLanding == null) {
            drawer.openDrawer(GravityCompat.END);
        }

        if (intent.getBooleanExtra("from_load", false))
            onActivityResult(REQUEST_LOGIN_NEW_SITE, RESULT_OK, intent);

        fillRightNavMenu();

        View leftNavMenuHeaderView = getLayoutInflater().inflate(R.layout.left_nav_menu_header, leftNavMenu, false);
        if (leftNavMenuHeaderView != null) {

            leftNavMenu.addHeaderView(leftNavMenuHeaderView);
            curLandingTextView = (TextView) leftNavMenuHeaderView.findViewById(R.id.left_nav_menu_top_site_address);
            curLandingFaviconView = (ImageView) leftNavMenuHeaderView.findViewById(R.id.left_nav_menu_top_landing_favicon);
            curLandingTextView.setTextColor(Color.BLACK);
        }

        View rightNavMenuHeaderView = getLayoutInflater().inflate(R.layout.right_nav_menu_header, rightNavMenu, false);
        if (rightNavMenuHeaderView != null) {

            rightNavMenu.addHeaderView(rightNavMenuHeaderView);
            rightNavMenuHeaderView.findViewById(R.id.but_add_site).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivityForResult(new Intent(core.context, LoginActivity.class), REQUEST_LOGIN_NEW_SITE);
                }
            });
        }
        intent.removeExtra("from_load");
        intent.removeExtra("site_id");
    }

    //При возвращении из другой активности
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) {
            return;
        }

        switch (requestCode) {
            //При подключении нового сайта
            case REQUEST_LOGIN_NEW_SITE:
                int site_id = data.getIntExtra("site_id", -1);
                if (site_id == -1)
                    break;
                Site site = new Site(site_id);//Загружаем из БД
                core.sites.add(site);//Добавляем в массив загруженных сайтов
                break;
        }
    }

    //Заполнение правого меню сайтами и лендингами
    private void fillRightNavMenu() {
        Menu rightMenu = rightNavMenu.getMenu();
        for (Site site : core.sites) {
            rightMenu.add(++rightNavMenuGroup, 0, rightNavMenuOrder++, site.host).setIcon(R.drawable.ic_menu_expanded);
            site.group_id = rightNavMenuGroup;
            int li = 1;
            for (Landing land : site.landings)
                rightMenu.add(rightNavMenuGroup, li++, rightNavMenuOrder++, land.title).setIcon(land.icon);
        }
    }

    //При нажатии кнопки назад
    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
        }
    }

    //Заполняем верхнее меню
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.over_menu_items, menu);
        return true;
    }

    //При выборе пункта в верхнем меню
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.menu_reload:
                refreshCurFragment();
                break;
            case R.id.menu_settings:
                startActivity(new Intent(this, SettingsActivity.class));
                break;
            case R.id.menu_about:
                startActivity(new Intent(this, AboutActivity.class));
                break;
        }
        return true;
    }

    //Обновление текуущего фрагмента
    private void refreshCurFragment() {
        if (curFragment == null)
            return;
        //Посылаем в фрагмент команду обновиться
        if (curFragment instanceof InfoFragment) {
            infoFragment.refresh();
        } else if (curFragment instanceof WidgetsFragment) {
            widgetsFragment.refresh();
        } else if (curFragment instanceof LeadsFragment) {
            leadsFragment.refresh();
        } else if (curFragment instanceof StatisticsFragment) {
            statisticsFragment.refresh();
        }/* else if (curFragment instanceof StatisticsFragment) {
            statisticsFragment.refresh();
        } else if (curFragment instanceof StatisticsFragment) {
            statisticsFragment.refresh();
        } else if (curFragment instanceof StatisticsFragment) {
            statisticsFragment.refresh();
        }*/
    }

    //Переподключает текущий фрагмент
    public void reattachCurFragment() {
        getFragmentManager().beginTransaction().detach(curFragment).attach(curFragment).commit();
    }

    //Устанавливает новый текущий лендинг
    private void setCurLanding(int site_id, int landing_id) {
        core.curSite = core.sites.get(site_id);
        core.curLanding = core.sites.get(site_id).landings.get(landing_id);

        curLandingTextView.setText(core.curLanding.title);
        curLandingFaviconView.setImageDrawable(core.curLanding.icon);
    }

    public void setProgressBar(boolean visible) {
        View mf = findViewById(R.id.main_frame);
        if (mf == null)
            return;
        mf.setBackgroundColor(visible ? Color.argb(166, 133, 133, 133) : Color.argb(255, 255, 255, 255));
        View pb = findViewById(R.id.progress);
        if (pb == null)
            return;
        pb.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    //При выборе фрагмента в левом меню
    private class LeftNavigationMenuItemListener implements NavigationView.OnNavigationItemSelectedListener {

        @Override
        public boolean onNavigationItemSelected(MenuItem item) {
            //Если не выбран текущий лендинг
            if (core.curLanding == null) {
                //Просим его выбрать
                drawer.closeDrawer(GravityCompat.START);
                drawer.openDrawer(GravityCompat.END);
                return true;
            }

            curFragment = null;
            switch (item.getItemId()) {
                case R.id.menu_summary:
                    curFragment = infoFragment = (infoFragment != null) ? infoFragment : new InfoFragment();
                    toolbar.setTitle(Core.getString(R.string.menu_info));
                    break;
                case R.id.menu_widgets:
                    curFragment = widgetsFragment = (widgetsFragment != null) ? widgetsFragment : new WidgetsFragment();
                    toolbar.setTitle(Core.getString(R.string.menu_widgets));
                    break;
                case R.id.menu_leads:
                    curFragment = leadsFragment = (leadsFragment != null) ? leadsFragment : new LeadsFragment();
                    toolbar.setTitle(Core.getString(R.string.menu_leads));
                    break;
                case R.id.menu_statistics:
                    curFragment = statisticsFragment = (statisticsFragment != null) ? statisticsFragment : new StatisticsFragment();
                    toolbar.setTitle(Core.getString(R.string.menu_statistics));
                    break;

                case R.id.menu_yandex:
                    curFragment = yandexFragment = (yandexFragment != null) ? yandexFragment : new YandexFragment();
                    toolbar.setTitle(Core.getString(R.string.menu_yandex));
                    break;
                case R.id.menu_google:
                    curFragment = googleFragment = (googleFragment != null) ? googleFragment : new GoogleFragment();
                    toolbar.setTitle(Core.getString(R.string.menu_google));
                    break;
                case R.id.menu_vk:
                    curFragment = vkFragment = (vkFragment != null) ? vkFragment : new VKFragment();
                    toolbar.setTitle(Core.getString(R.string.menu_vk));
                    break;
                case R.id.menu_settings:
                    curFragment = settingsFragment = (settingsFragment != null) ? settingsFragment : new SettingsFragment();
                    toolbar.setTitle(Core.getString(R.string.menu_settings));
                    break;

            }
            //Устанавливаем новый фрагмент
            if (curFragment != null)
                getFragmentManager().beginTransaction().replace(R.id.main_frame, curFragment).commit();
            drawer.closeDrawer(GravityCompat.START);
            return true;
        }
    }

    //При выборе лендинга в правом меню
    private class RightNavigationMenuItemListener implements NavigationView.OnNavigationItemSelectedListener {

        @Override
        public boolean onNavigationItemSelected(MenuItem item) {
            if (item.getItemId() == 0) {//Если нажат элемент-сайт
                if (core.sites.get(item.getGroupId()).is_visible) {//Если был список раскрыт
                    rightNavMenu.getMenu().setGroupVisible(item.getGroupId(), false);//Скрываем
                    item.setVisible(true);
                    item.setIcon(R.drawable.ic_menu_collapsed);
                    core.sites.get(item.getGroupId()).is_visible = false;
                } else {//Если был список скрыт
                    rightNavMenu.getMenu().setGroupVisible(item.getGroupId(), true);//Раскрываем
                    item.setVisible(true);
                    item.setIcon(R.drawable.ic_menu_expanded);
                    core.sites.get(item.getGroupId()).is_visible = true;
                }
            } else {//Если нажат элемент-лендинг
                setCurLanding(item.getGroupId(), item.getItemId() - 1);
                if (curFragment == null) {//Если фрагмент не выбран, подключим сводку
                    curFragment = infoFragment = (infoFragment != null) ? infoFragment : new InfoFragment();
                    getFragmentManager().beginTransaction().replace(R.id.main_frame, curFragment).commit();
                } else {//Иначе, перезагрузим его
                    reattachCurFragment();
                }
                drawer.closeDrawer(GravityCompat.END);
            }
            return true;
        }
    }
}
