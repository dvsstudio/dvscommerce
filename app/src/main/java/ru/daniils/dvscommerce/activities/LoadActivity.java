package ru.daniils.dvscommerce.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import ru.daniils.dvscommerce.Core;

/*
Запускается при включении приложения.
При отсутствии записей в бд перенаправляет на логин.
Иначе запускает MainActivity.
*/
public class LoadActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Core core = Core.getInstance();
        core.start(this);
        core.setActivity(this);
        core.loadSites();
        if (core.sites.size() == 0) {
            startActivity(new Intent(this, LoginActivity.class));
        } else {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }
    }
}
