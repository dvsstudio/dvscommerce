package ru.daniils.dvscommerce.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import ru.daniils.dvscommerce.Core;
import ru.daniils.dvscommerce.R;
import ru.daniils.dvscommerce.Utils;
import ru.daniils.dvscommerce.entities.Site;

import static ru.daniils.dvscommerce.Core.makeToast;

/*
Позволяет подключиться к сайту и получить токен для дальнейших операций.
*/
public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText inHost, inEmail, inPass;
    private ProgressBar progress;
    private AppCompatActivity selfActivity = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Core.getInstance().setActivity(this);
        inHost = (EditText) findViewById(R.id.loginHost);
        inEmail = (EditText) findViewById(R.id.loginLEmail);
        inPass = (EditText) findViewById(R.id.loginPassword);
        progress = (ProgressBar) findViewById(R.id.progress);

        Button butEnter = (Button) findViewById(R.id.loginEnter);
        if (butEnter != null)
            butEnter.setOnClickListener(this);

        View login1, login2;
        if ((login1 = findViewById(R.id.login1)) != null)
            login1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    inHost.setText("land.daniils.ru");
                    inEmail.setText("email@dvs.ru");
                    inPass.setText("152263");
                }
            });
        if ((login2 = findViewById(R.id.login2)) != null)
            login2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    inHost.setText("land2.daniils.ru");
                    inEmail.setText("email@dvs.ru");
                    inPass.setText("152263");
                }
            });

        selfActivity = this;
    }

    @Override
    public void onClick(View v) {
        //Получаем данные из полей
        String host = inHost.getText().toString();//"land.daniils.ru";//
        String email = inEmail.getText().toString();//"danstrrr@yandex.ru";//
        String pass = Utils.MD5(inPass.getText().toString());//Utils.MD5("dan152463");//
        //Асинхронная задача по подключению к хосту, дабы не зависал интерфейс
        new AsyncTask<String, Void, String>() {
            String host, email, pass;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progress.setVisibility(View.VISIBLE);//Показываем прогрес-бар
                //Запрещаем дальнейшее редактирование полей
                inHost.setEnabled(false);
                inEmail.setEnabled(false);
                inPass.setEnabled(false);
            }

            @Override
            protected String doInBackground(String... params) {
                host = params[0];
                email = params[1];
                pass = params[2];
                return Core.loginToHost(host, email, pass);
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                //Отлавливаем ошибки подключения и прочие
                if (response != null && response.contains("Error: ")) {
                    makeToast(R.string.enter_wrong_pass, true);
                    response = null;
                } else if (response != null && !Utils.isValidMD5(response)) {
                    makeToast(R.string.enter_failed, false);
                    response = null;
                }
                if (response == null) {//При ошибке
                    progress.setVisibility(View.INVISIBLE);//Скрываем прогрес-бар
                    //Возвращаем пользователю доступ к полям
                    inHost.setEnabled(true);
                    inEmail.setEnabled(true);
                    inPass.setEnabled(true);
                    return;
                }
                int id = Site.addSite(host, email, pass, response);//Добавляем сайт в БД
                //Переходим (обратно) в MainActivity
                Intent intent = new Intent(selfActivity, MainActivity.class);
                intent.putExtra("site_id", id);
                setResult(RESULT_OK, intent);
                if (!(Core.getInstance().activity instanceof MainActivity)) {
                    intent.putExtra("from_load", true);
                    startActivity(intent);
                }
                selfActivity.finish();
            }
        }.execute(host, email, pass);
    }
}
