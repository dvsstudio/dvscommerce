package ru.daniils.dvscommerce.activities;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import ru.daniils.dvscommerce.Core;
import ru.daniils.dvscommerce.R;

/*
Активность "О приложении"
 */
public class AboutActivity extends AppCompatActivity implements View.OnClickListener {
    private int count_hon = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_about);
        ((TextView) findViewById(R.id.about_version)).setText(String.format("Version %s", Core.version));//Устанавливаем версию
        findViewById(R.id.about_icon).setOnClickListener(this);//Пасхалка
    }

    @Override
    public void onClick(View v) {
        //Пасхалка {
        if (count_hon == 10) {
            Core.makeToast("DVS's sexy and he know it...", true);
            count_hon = 0;
        } else {
            count_hon++;
        }
        // }
    }
}
