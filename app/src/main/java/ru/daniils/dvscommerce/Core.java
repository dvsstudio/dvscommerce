package ru.daniils.dvscommerce;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.Map;

import ru.daniils.dvscommerce.activities.MainActivity;
import ru.daniils.dvscommerce.entities.Landing;
import ru.daniils.dvscommerce.entities.Site;
import ru.daniils.dvscommerce.entities.Statistic;
import ru.daniils.dvscommerce.entities.Widget;

/*
Ядро системы.
Объединяет все компоненты.
Дает доступ к функциям.
 */
public class Core {
    public final static String version = "1.0";
    private static Core ourInstance = null;
    public ArrayList<Site> sites;
    public Context context;
    public Activity activity;
    public Site curSite;
    public Landing curLanding;
    public Widget curWidget;
    public Statistic curStatistic;
    private DB db = null;
    private Connection conn = null;
    private SharedPreferences prefs = null;

    private Core() {

    }

    public static Core getInstance() {
        if (ourInstance == null)
            ourInstance = new Core();
        return ourInstance;
    }

    //Позволяет получить информацию с сайты по имени хоста и токену
    public static String get(String host, String token, Map<String, String> data) {
        Connection.POSTQuery query = new Connection.POSTQuery(host, "get");
        query.put("token", token);
        if (data != null) {
            for (String s : data.keySet()) {
                query.put(s, data.get(s));
            }
        }
        query.send();
        String result = query.getResult();
        CheckConnectionFails ccf = new CheckConnectionFails(result);
        Core.getInstance().activity.runOnUiThread(ccf);
        return ccf.getResult();
    }

    //Позволяет установить параметры data на сайте по имени хоста и токену
    public static String set(String host, String token, Map<String, String> data) {
        Connection.POSTQuery query = new Connection.POSTQuery(host, "set");
        query.put("token", token);
        if (data != null) {
            for (String s : data.keySet()) {
                query.put(s, data.get(s));
            }
        }
        query.send();
        String result = query.getResult();
        CheckConnectionFails ccf = new CheckConnectionFails(result);
        Core.getInstance().activity.runOnUiThread(ccf);
        return ccf.getResult();
    }

    //Простое получение строки из ресурсов приложения
    public static String getString(int res_id) {
        return Core.getInstance().context.getResources().getString(res_id);
    }

    //Получает(создает) кэш-папку приложения (...(SDCARD).../Android/data/ru.daniils.dvscommerce/)
    public static String getStorageDir() {
        if (ourInstance == null)
            return "";
        File file = ourInstance.context.getExternalFilesDir(null);
        new File(file, "favicons").mkdirs();
        return file != null ? file.getPath() : null;
    }

    //Позволяет загрузить файл с сайта по имени хоста и расположения на нем и сохранить в некоторую папку на устройстве
    public static File loadFile(String host, String source, String dest) {
        Connection.FILEQuery fq = new Connection.FILEQuery(host + source, getStorageDir() + dest);
        fq.get();
        return fq.getResult();
    }

    //Показывает Toast уведомление из строки
    public static void makeToast(final String message, final boolean is_long) {
        Toast.makeText(Core.getInstance().context, message, is_long ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT).show();
    }

    //Показывает Toast уведомление из ресурса приложения
    public static void makeToast(final int res_id, final boolean is_long) {
        Toast.makeText(Core.getInstance().context, Core.getString(res_id), is_long ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT).show();
    }

    //Позволяет подключиться к хосту по имени хоста, логину и MD5-кешу пароля для получения токена
    public static String loginToHost(String host, String email, String passwordMD5) {
        Connection.POSTQuery query = new Connection.POSTQuery(host, "auth");
        query.put("email", email);
        query.put("password", passwordMD5);
        query.send();
        String result = query.getResult();
        CheckConnectionFails ccf = new CheckConnectionFails(result);
        Core.getInstance().activity.runOnUiThread(ccf);
        return ccf.getResult();
    }

    //Запускает соединение и БД приложения
    public void start(Context _context) {
        if (_context == null)
            return;
        context = _context;
        if (conn == null)//Соединения
            conn = Connection.getInstance();
        if (db == null) {//БД
            db = DB.getInstance();
            db.setContext(context);
            db.connect();
        }
        if (prefs == null)//Настройки
            prefs = PreferenceManager.getDefaultSharedPreferences(context);

    }

    //Устанавливает текущее активити
    public void setActivity(Activity _activity) {
        if (activity != null && activity instanceof MainActivity)
            return;
        activity = _activity;
        context = _activity;
    }

    //Загружает сохраненные сайты в память
    public void loadSites() {
        sites = Site.getSites();
        if (sites == null)
            sites = new ArrayList<>();
    }

    public void setProgressBar(boolean visible) {
        if (activity == null || !(activity instanceof MainActivity))
            return;
        ((MainActivity) activity).setProgressBar(visible);
    }

    //Класс проверяет строку (ответ от сервера) на ошибки и показывает уведомление при их наличии.
    //Завершает работу текущего соединения.
    private static class CheckConnectionFails extends Thread {
        private String result;
        private boolean ended = false;

        CheckConnectionFails(String _result) {
            result = _result;
        }

        public void run() {
            if (result == null || result.equals("NULL")) {
                makeToast(R.string.no_internet, true);
                result = null;
            } else if (result.contains("RC: ")) {
                makeToast(String.format("%s: %s", Core.getString(R.string.enter_error), result.substring(4)), false);
                result = null;
            } else if (result.contains("<html")) {
                makeToast(String.format("%s Wrong request", Core.getString(R.string.enter_error)), false);
                result = null;
            } else if (result.equals("false")) {
                makeToast(R.string.no_more_content, false);
                result = null;
            }
            ended = true;
        }

        String getResult() {
            //"Правильный" join и возврат результата
            try {
                while (!ended)
                    Thread.sleep(300);
                join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return result;
        }

    }
}