package ru.daniils.dvscommerce.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import ru.daniils.dvscommerce.R;
import ru.daniils.dvscommerce.entities.Widget;

/*
Адаптер виджетов
 */
public class WidgetsAdapter extends BaseAdapter {
    public ArrayList<Widget> widgets;
    private LayoutInflater lInflater;

    public WidgetsAdapter(Context context, ArrayList<Widget> _widgets) {
        widgets = _widgets;
        lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    //Текущее кол-во элементов
    @Override
    public int getCount() {
        return widgets.size();
    }

    //Получение элемента
    @Override
    public Object getItem(int position) {
        return widgets.get(position);
    }

    //Получение ID элемента
    @Override
    public long getItemId(int position) {
        return position;
    }

    //Получение View элемента
    @Override
    public View getView(int position, View viewF, ViewGroup parent) {

        final View view;
        if (viewF == null) {//Если новый View, создаем его
            view = lInflater.inflate(R.layout.widget_row, parent, false);
        } else {//Если старый, очищаем от старого(приводим в порядок)
            view = viewF;
        }

        final Widget w = widgets.get(position);//Получаем виджет

        ((TextView) view.findViewById(R.id.widget_title)).setText(w.title);//Устанавливаем название
        ((TextView) view.findViewById(R.id.widget_type)).setText(w.w_type_title);//Устанавливаем тип
        ((TextView) view.findViewById(R.id.widget_position)).setText(String.format("%s (%s)", w.position, w.order));//Устанавливаем позицию и порядок

        return view;
    }

}
