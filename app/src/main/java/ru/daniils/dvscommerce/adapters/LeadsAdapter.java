package ru.daniils.dvscommerce.adapters;

import android.content.Context;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

import ru.daniils.dvscommerce.R;
import ru.daniils.dvscommerce.entities.Lead;

/*
Адаптер лидов.
 */
public class LeadsAdapter extends BaseAdapter {
    public ArrayList<Lead> leads;
    private LayoutInflater lInflater;

    public LeadsAdapter(Context context, ArrayList<Lead> _leads) {
        leads = _leads;
        lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    //Текущее кол-во элементов
    @Override
    public int getCount() {
        return leads.size();
    }

    //Получение элемента
    @Override
    public Object getItem(int position) {
        return leads.get(position);
    }

    //Получение ID элемента
    @Override
    public long getItemId(int position) {
        return position;
    }

    //Получение View элемента
    @Override
    public View getView(int position, View viewF, ViewGroup parent) {

        final View view;
        ImageButton apply, refuse, call;
        if (viewF == null) {//Если новый View, создаем его
            view = lInflater.inflate(R.layout.lead_row, parent, false);
            apply = (ImageButton) view.findViewById(R.id.lead_apply_but);
            refuse = (ImageButton) view.findViewById(R.id.lead_refuse_but);
            call = (ImageButton) view.findViewById(R.id.lead_call_but);
        } else {//Если старый, очищаем от старого(приводим в порядок)
            view = viewF;
            apply = (ImageButton) view.findViewById(R.id.lead_apply_but);
            refuse = (ImageButton) view.findViewById(R.id.lead_refuse_but);
            call = (ImageButton) view.findViewById(R.id.lead_call_but);
            apply.setVisibility(View.VISIBLE);
            refuse.setVisibility(View.VISIBLE);
            view.setBackgroundResource(R.drawable.row_bb);
        }

        final Lead l = leads.get(position);//Получаем лида

        ((TextView) view.findViewById(R.id.lead_name)).setText(l.name);//Устанавливаем имя
        ((TextView) view.findViewById(R.id.lead_phone)).setText(l.phone);//Устанавливаем номер телефона

        //Проверяем статус
        switch (l.status) {
            case 1://Принятый лид
                apply.setVisibility(View.INVISIBLE);
                refuse.setVisibility(View.VISIBLE);
                view.setBackgroundResource(R.drawable.row_bb_l_green);
                break;
            case 2://Отмененный лид
                apply.setVisibility(View.VISIBLE);
                refuse.setVisibility(View.INVISIBLE);
                view.setBackgroundResource(R.drawable.row_bb_l_red);
                break;
            case 3://Требующий отметки лид
                apply.setVisibility(View.VISIBLE);
                refuse.setVisibility(View.VISIBLE);
                view.setBackgroundResource(R.drawable.row_bb_l_blue);
                break;
        }
        //Устанавливаем слушатели нажатий на кнопки
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callLead(l, view);
            }
        });
        apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                applyLead(l, view);
            }
        });
        refuse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refuseLead(l, view);
            }
        });

        return view;
    }

    //Звоним лиду
    private void callLead(Lead lead, View view) {
        lead.call();
        view.findViewById(R.id.lead_apply_but).setVisibility(View.VISIBLE);
        view.findViewById(R.id.lead_refuse_but).setVisibility(View.VISIBLE);
        view.setBackgroundResource(R.drawable.row_bb_l_blue);
    }

    //Принимаем лида
    private void applyLead(Lead lead, View view) {
        SetLeadStatus sls = new SetLeadStatus();
        sls.lead = lead;
        sls.view = view;
        sls.action = true;
        sls.execute();
    }

    //Отклоняем лида
    private void refuseLead(Lead lead, View view) {
        SetLeadStatus sls = new SetLeadStatus();
        sls.lead = lead;
        sls.view = view;
        sls.action = false;
        sls.execute();
    }

    //Асинхронный класс отправки нового статуса лида на сайт
    private class SetLeadStatus extends AsyncTask<Void, Void, String> {
        Lead lead;
        View view;
        /*
        true=принимаем лида
        false=отклоняем лида
         */
        boolean action;

        @Override
        protected String doInBackground(Void... arg) {
            if (action)
                return lead.apply();
            else
                return lead.refuse();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result == null || !result.equals("OK") || view == null)
                return;
            //Меняем вид View
            if (action) {
                view.findViewById(R.id.lead_apply_but).setVisibility(View.INVISIBLE);
                view.findViewById(R.id.lead_refuse_but).setVisibility(View.VISIBLE);
                view.setBackgroundResource(R.drawable.row_bb_l_green);
            } else {
                view.findViewById(R.id.lead_apply_but).setVisibility(View.VISIBLE);
                view.findViewById(R.id.lead_refuse_but).setVisibility(View.INVISIBLE);
                view.setBackgroundResource(R.drawable.row_bb_l_red);
            }
        }
    }
}
