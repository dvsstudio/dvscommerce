package ru.daniils.dvscommerce.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ru.daniils.dvscommerce.Core;
import ru.daniils.dvscommerce.R;
import ru.daniils.dvscommerce.entities.Landing;

/*
Заготовка для фрагмента Google Analytics
 */
public class GoogleFragment extends Fragment {
    private Landing landing;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_coming, container, false);
        Core core = Core.getInstance();
        landing = core.curLanding;
        return view;
    }

    public void refresh() {

    }

}