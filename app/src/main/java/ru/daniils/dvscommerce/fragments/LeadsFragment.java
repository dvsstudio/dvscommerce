package ru.daniils.dvscommerce.fragments;

import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import ru.daniils.dvscommerce.Core;
import ru.daniils.dvscommerce.R;
import ru.daniils.dvscommerce.adapters.LeadsAdapter;
import ru.daniils.dvscommerce.entities.Landing;

/*
Фрагмент лидов
 */
public class LeadsFragment extends Fragment {

    private LeadsAdapter leadsAdapter;
    private Landing landing;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_content_list, container, false);
        Core core = Core.getInstance();
        landing = core.curLanding;
        ListView contentView = (ListView) view.findViewById(R.id.content_list);

        leadsAdapter = new LeadsAdapter(core.context, landing.leads);
        contentView.setAdapter(leadsAdapter);

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                Core.getInstance().setProgressBar(true);
            }

            @Override
            protected Void doInBackground(Void... arg) {
                landing.loadLeads();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                Core.getInstance().activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        leadsAdapter.leads = landing.leads;
                        leadsAdapter.notifyDataSetChanged();
                    }
                });
                Core.getInstance().setProgressBar(false);
            }
        }.execute();

        return view;
    }

    public void refresh() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                Core.getInstance().setProgressBar(true);
            }

            @Override
            protected Void doInBackground(Void... params) {
                landing.loadLeads(landing.last_lead_id);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                Core.getInstance().activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        leadsAdapter.leads = landing.leads;
                        leadsAdapter.notifyDataSetChanged();
                    }
                });
                Core.getInstance().setProgressBar(false);
            }
        }.execute();

    }
}