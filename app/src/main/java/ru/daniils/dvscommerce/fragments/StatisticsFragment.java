package ru.daniils.dvscommerce.fragments;

import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ru.daniils.dvscommerce.Core;
import ru.daniils.dvscommerce.R;
import ru.daniils.dvscommerce.Utils;
import ru.daniils.dvscommerce.entities.Landing;
import ru.daniils.dvscommerce.entities.Statistic;
import ru.daniils.dvscommerce.view.StatisticsView;

public class StatisticsFragment extends Fragment {
    StatisticsView stat;
    private Statistic today;
    private Landing landing;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_statistics, container, false);
        Core core = Core.getInstance();
        landing = core.curLanding;
        stat = (StatisticsView) view.findViewById(R.id.stat);
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                Core.getInstance().setProgressBar(true);
            }
            @Override
            protected Void doInBackground(Void... arg) {
                landing.loadStatistics();
                today = landing.statistics.get(Utils.timeToDate(null));
                if (today == null)
                    today = landing.statistics.get(Utils.timeToDate(System.currentTimeMillis() - 24 * 60 * 60 * 1000));
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (today == null)
                    return;
                stat.setStatistics(today);
                stat.invalidate();
                Core.getInstance().setProgressBar(false);
            }
        }.execute();
        return view;
    }

    public void refresh() {
        if (today != null)
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    Core.getInstance().setProgressBar(true);
                }

                @Override
                protected Void doInBackground(Void... params) {
                    landing.loadStatistics(landing.last_statistics_id);
                    today = landing.statistics.get(Utils.timeToDate(null));
                    if (today == null)
                        today = landing.statistics.get(Utils.timeToDate(System.currentTimeMillis() - 24 * 60 * 60 * 1000));
                    if (today != null)
                        today.reload();
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    if (today == null)
                        return;
                    stat.setStatistics(today);
                    stat.invalidate();
                    Core.getInstance().setProgressBar(false);
                }
            }.execute();
    }

}