package ru.daniils.dvscommerce.fragments;

import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ru.daniils.dvscommerce.Core;
import ru.daniils.dvscommerce.R;
import ru.daniils.dvscommerce.Utils;
import ru.daniils.dvscommerce.entities.Landing;
import ru.daniils.dvscommerce.entities.Statistic;

/*
Фрагмент сводки информации по лендингу
 */
public class InfoFragment extends Fragment {
    private View mainView;
    private Statistic today, prev;
    private Landing landing;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_info, container, false);
        landing = Core.getInstance().curLanding;

        //Асинхронно загрузить статистику из БД
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                Core.getInstance().setProgressBar(true);
            }

            @Override
            protected Void doInBackground(Void... arg) {
                landing.loadStatistics();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                today = landing.statistics.get(Utils.timeToDate(null));
                showToday();
                prev = landing.statistics.get(Utils.timeToDate(System.currentTimeMillis() - 24 * 60 * 60 * 1000));
                showPrev();
                Core.getInstance().setProgressBar(false);
            }
        }.execute();

        return mainView;
    }

    public void refresh() {
        //Асинхронно загрузить последнюю статистику с сайта
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                Core.getInstance().setProgressBar(true);
            }

            @Override
            protected Void doInBackground(Void... params) {
                landing.loadStatistics(landing.last_statistics_id);
                today = landing.statistics.get(Utils.timeToDate(null));
                if (today != null)
                    today.reload();
                prev = landing.statistics.get(Utils.timeToDate(System.currentTimeMillis() - 24 * 60 * 60 * 1000));
                if (prev != null)
                    prev.reload();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                showToday();
                showPrev();
                Core.getInstance().setProgressBar(false);
            }
        }.execute();
    }

    private void showToday() {
        ((TextView) mainView.findViewById(R.id.info_visits_today)).setText(String.format("%d", (today == null) ? 0 : today.visits));
        ((TextView) mainView.findViewById(R.id.info_views_today)).setText(String.format("%d", (today == null) ? 0 : today.views));
        ((TextView) mainView.findViewById(R.id.info_leads_today)).setText(String.format("%d", (today == null) ? 0 : today.leads));
    }

    private void showPrev() {
        if (prev == null) {
            mainView.findViewById(R.id.info_byprev).setVisibility(View.GONE);
            return;
        }
        ((TextView) mainView.findViewById(R.id.info_visits_prev)).setText(String.format("%d", prev.visits));
        ((TextView) mainView.findViewById(R.id.info_views_prev)).setText(String.format("%d", prev.views));
        ((TextView) mainView.findViewById(R.id.info_leads_prev)).setText(String.format("%d", prev.leads));
    }

}