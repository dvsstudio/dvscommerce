package ru.daniils.dvscommerce.fragments;

import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import ru.daniils.dvscommerce.Core;
import ru.daniils.dvscommerce.R;
import ru.daniils.dvscommerce.adapters.WidgetsAdapter;
import ru.daniils.dvscommerce.entities.Landing;

public class WidgetsFragment extends Fragment {

    private WidgetsAdapter widgetsAdapter;
    private Landing landing;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_content_list, container, false);
        Core core = Core.getInstance();
        landing = core.curLanding;
        ListView contentView = (ListView) view.findViewById(R.id.content_list);

        widgetsAdapter = new WidgetsAdapter(core.context, landing.widgets);
        contentView.setAdapter(widgetsAdapter);

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                Core.getInstance().setProgressBar(true);
            }

            @Override
            protected Void doInBackground(Void... params) {
                landing.loadWidgets();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                Core.getInstance().activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        widgetsAdapter.widgets = landing.widgets;
                        widgetsAdapter.notifyDataSetChanged();
                    }
                });
                Core.getInstance().setProgressBar(false);
            }
        }.execute();

        return view;
    }

    public void refresh() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                Core.getInstance().setProgressBar(true);
            }

            @Override
            protected Void doInBackground(Void... params) {
                landing.loadWidgets(landing.last_widget_id);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                Core.getInstance().activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        widgetsAdapter.widgets = landing.widgets;
                        widgetsAdapter.notifyDataSetChanged();
                    }
                });
                Core.getInstance().setProgressBar(false);
            }
        }.execute();

    }
}