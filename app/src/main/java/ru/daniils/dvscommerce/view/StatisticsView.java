package ru.daniils.dvscommerce.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

import ru.daniils.dvscommerce.Utils;
import ru.daniils.dvscommerce.entities.Statistic;

public class StatisticsView extends View {

    private Statistic stat = null;
    private Paint p;
    private int max = 0, min = Integer.MAX_VALUE;

    private int width, height;

    //Стандартные конструкторы
    public StatisticsView(Context context) {
        super(context);
        load();
    }

    public StatisticsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        load();
    }

    public StatisticsView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        load();
    }

    //Устанавливаем размер текста, чтобы получить нужную ширину
    private static void setTextSizeForWidth(Paint paint, float desiredWidth, String text) {
        final float testTextSize = 48f;
        paint.setTextSize(testTextSize);
        Rect bounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), bounds);
        float desiredTextSize = testTextSize * desiredWidth / bounds.width();
        paint.setTextSize(desiredTextSize);
    }

    private void load() {
        p = new Paint();
        p.setFlags(Paint.ANTI_ALIAS_FLAG);//Устанавливаем сглаживание
        p.setStrokeWidth(5);//Устанавливаем толщину линий
    }

    //Устанавливаем текущий день статистики
    public void setStatistics(Statistic _stat) {
        if (_stat == null)
            return;
        //Размеры обасти рисования
        width = getWidth() - 10;
        height = getHeight() - 80;
        setTextSizeForWidth(p, getWidth() / 12, "#####");//Устанавливаем размер текста
        stat = _stat;

        //Ищем максимальное и минимальное значения
        for (int i = 0; i < 12; i++) {
            max = Math.max(max, stat.hours[i]);
            min = Math.min(min, stat.hours[i]);
        }
    }

    //Рисуем график
    @Override
    public void draw(Canvas c) {
        super.draw(c);

        if (stat == null)
            return;
        //Рисуем график из линий
        p.setColor(Color.argb(255, 33, 148, 211));
        for (int i = 0; i < 11; i++)
            drawLine(width / 12 * i, height / max * stat.hours[i], width / 12 * (i + 1), height / max * stat.hours[i + 1], c);

        int hour = Utils.timeToHour(null);
        hour = (hour - hour % 2) / 2;
        for (int i = 0; i < 12; i++) {
            if (i == hour)//Если i = текущий час
                p.setColor(Color.argb(255, 51, 204, 0));
            else if (stat.hours[i] == max)//Максимальное кол-во посетителей
                p.setColor(Color.argb(255, 204, 51, 51));
            else//"Типичный" час
                p.setColor(Color.argb(255, 23, 102, 145));
            //Поверх рисуем точки
            drawCircle(width / 12 * i, height / max * stat.hours[i], 10, c);
            p.setColor(Color.BLACK);
            //Пишем кол-во посетителей
            if (stat.hours[i] != 0)
                drawText(width / 12 * i + 25, height / max * stat.hours[i] + 40, stat.hours[i] + "", c);
            //Пишем время
            drawText(width / 12 * i, 0, i * 2 + "-" + (i * 2 + 2), c);
        }
    }

    //Упрощенный функции рисования
    private void drawLine(int x, int y, int x2, int y2, Canvas c) {
        c.drawLine(x + 10, getHeight() - y - 40, x2 + 10, getHeight() - y2 - 40, p);
    }

    private void drawCircle(int x, int y, int r, Canvas c) {
        c.drawCircle(x + 10, getHeight() - y - 40, r, p);
    }

    private void drawText(int x, int y, String s, Canvas c) {
        c.drawText(s, x, getHeight() - y - 5, p);
    }
}
