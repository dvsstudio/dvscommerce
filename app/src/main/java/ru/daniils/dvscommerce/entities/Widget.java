package ru.daniils.dvscommerce.entities;

import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import ru.daniils.dvscommerce.Core;
import ru.daniils.dvscommerce.DB;
import ru.daniils.dvscommerce.R;

/*
Сущность виджета
 */
public class Widget {
    public int id;
    public String w_type_title;
    public String title;
    public String options;
    public String position;
    public int order;
    private String widget_name;
    private Landing landing;
    private int original_id;

    private Widget(Cursor c, Landing _landing) {
        load(c);
        landing = _landing;
    }

    //Получение загруженных виджетов из БД
    static ArrayList<Widget> getWidgets(Landing land, String landing_name) {
        ArrayList<Widget> ret = new ArrayList<>();
        HashMap<String, ArrayList<Widget>> bigTemp = new HashMap<>();
        for (String pos : land.positions)
            bigTemp.put(pos, new ArrayList<Widget>());

        Cursor c = DB.query("landing_widgets", null, "site_id=" + land.site.id + " AND landing_name='" + landing_name + "'", null, null, null, "`order` ASC");
        if (c == null || !c.moveToFirst()) return ret;
        do {
            Widget wTemp = new Widget(c, land);
            ArrayList<Widget> alwTemp = bigTemp.get(wTemp.position);
            alwTemp.add(wTemp);
            bigTemp.put(wTemp.position, alwTemp);
        } while (c.moveToNext());
        c.close();
        for (String pos : land.positions)
            ret.addAll(bigTemp.get(pos));
        return ret;
    }

    //Добавление виджета в БД
    static int addWidget(Map<String, String> widget) {
        return (int) DB.insert("landing_widgets", widget);
    }

    //Удаление виджета из БД по ID
    static boolean removeById(int id) {
        return DB.removeById("landing_widgets", id) == 1;
    }

    //Получение Человеко-понятного-названия-типа-виджета
    public static String getWidgetTypeTitle(String name) {
        String ret = name;
        Context cont = Core.getInstance().context;
        switch (name) {
            case "title":
                ret = cont.getString(R.string.widget_title);
                break;
            case "text":
                ret = cont.getString(R.string.widget_text);
                break;
            case "form":
                ret = cont.getString(R.string.widget_form);
                break;
            case "review":
                ret = cont.getString(R.string.widget_review);
                break;
            case "trigger":
                ret = cont.getString(R.string.widget_trigger);
                break;
            case "social":
                ret = cont.getString(R.string.widget_social);
                break;
        }
        return ret;
    }

    //Загрузка основных полей лида из БД по Cursor
    private void load(Cursor c) {
        id = c.getInt(c.getColumnIndex("id"));
        original_id = c.getInt(c.getColumnIndex("original_id"));
        widget_name = c.getString(c.getColumnIndex("widget_name"));
        w_type_title = getWidgetTypeTitle(widget_name);
        title = c.getString(c.getColumnIndex("title"));
        options = c.getString(c.getColumnIndex("options"));
        position = c.getString(c.getColumnIndex("position"));
        order = c.getInt(c.getColumnIndex("order"));
    }
}
