package ru.daniils.dvscommerce.entities;


import android.database.Cursor;
import android.graphics.drawable.Drawable;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import ru.daniils.dvscommerce.Core;
import ru.daniils.dvscommerce.DB;

/*
Лендинг - класс, собирающий большую часть функций по работе с получением данных.
 */
public class Landing {
    public int id;
    public String name;
    public String title;
    public Drawable icon;
    public int last_widget_id, last_lead_id, last_statistics_id;
    public ArrayList<Widget> widgets;
    public ArrayList<Lead> leads;
    public HashMap<String, Statistic> statistics;
    Site site;
    ArrayList<String> positions;
    private String options;
    private boolean published;

    private Landing(Cursor c, Site _site) {
        site = _site;
        last_widget_id = 0;
        last_statistics_id = 0;
        last_lead_id = 0;
        positions = new ArrayList<>();
        widgets = new ArrayList<>();
        leads = new ArrayList<>();
        statistics = new HashMap<>();
        load(c);
    }

    Landing() {
    }

    //Удаление лендинга из БД по ID
    private static boolean removeById(int id) {
        return DB.removeById("landings", id) == 1;
    }

    //Получение загруженных лендингов из БД
    static ArrayList<Landing> getLandings(Site site) {
        ArrayList<Landing> ret = new ArrayList<>();

        Cursor c = DB.query("landings", null, "site_id=" + site.id, null, null, null, "title ASC");
        if (c != null && !c.moveToFirst()) return ret;
        do {
            ret.add(new Landing(c, site));
        } while (c != null && c.moveToNext());
        if (c != null)
            c.close();
        return ret;
    }

    //Добавление лендинга в БД
    static int addLanding(Map<String, String> landing) {
        return (int) DB.insert("landings", landing);
    }

    //Загрузка основных полей лендинга из БД по Cursor
    private void load(Cursor c) {
        id = c.getInt(c.getColumnIndex("id"));
        name = c.getString(c.getColumnIndex("name"));
        title = c.getString(c.getColumnIndex("title"));
        options = c.getString(c.getColumnIndex("options"));
        published = c.getInt(c.getColumnIndex("published")) == 1;
        last_lead_id = c.getInt(c.getColumnIndex("last_lead_id"));
        last_widget_id = c.getInt(c.getColumnIndex("last_widget_id"));
        last_statistics_id = c.getInt(c.getColumnIndex("last_statistics_id"));
        String poss = c.getString(c.getColumnIndex("positions"));
        positions = (poss == null) ? new ArrayList<String>() : new ArrayList<>(Arrays.asList(poss.split("\\|")));
        icon = Drawable.createFromPath(Core.getStorageDir() + "/favicons/f" + site.id + "-" + name + ".ico");
    }

    //Стартовая загрузка виджетов лендинга
    public void loadWidgets() {
        if (widgets.size() == 0)//Если выполняется первый раз size=0
            widgets = Widget.getWidgets(this, name);//Пробуем получить виджеты из БД
        if (widgets.size() == 0)//При отсутствии загруженных
            loadWidgets(0);//Получаем с сайта начиная с ID 0
    }

    //Загрузка всех виджетов начиная с last_id
    public void loadWidgets(int last_id) {
        //Заполняем массив POST-запроса
        HashMap<String, String> data = new HashMap<>();
        data.put("what", "land_widgets");//Что хотим получить
        data.put("since", last_id + "");//С какого ID
        data.put("name", name);//Какой лендинг
        String result = Core.get(site.host, site.token, data);//Посылаем запрос на сайт
        //При ошибке завершаем работу
        if (result == null)
            return;
        if (last_id == 0)//Если загружаем заново (полностью), обнуляем старые данные в БД
            DB.delete("landing_widgets", "site_id=" + site.id + " AND landing_name='" + name + "'", null);
        try {
            //Распарсиваем полученную JSON-строку
            JSONObject node_root = new JSONObject(result);
            Iterator<String> keys = node_root.keys();
            if (keys == null)
                return;
            //Пока есть записи
            while (keys.hasNext()) {
                JSONObject widg = node_root.getJSONObject(keys.next());
                //Заполняем ассоциативный массив
                HashMap<String, String> map = new HashMap<>();
                map.put("original_id", widg.getInt("id") + "");
                map.put("site_id", site.id + "");
                map.put("landing_name", name);
                map.put("widget_name", widg.getString("widget_name"));
                map.put("title", widg.getString("title"));
                map.put("options", widg.getString("options"));
                map.put("position", widg.getString("position"));
                map.put("`order`", widg.getInt("order") + "");

                Widget.addWidget(map);//Добавляем в БД
                last_widget_id = Math.max(last_widget_id, widg.getInt("id"));//Запоминаем последний ID
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        widgets = Widget.getWidgets(this, name);//Получаем виджеты из БД
        //Сохраняем последний ID
        HashMap<String, String> map = new HashMap<>();
        map.put("last_widget_id", last_widget_id + "");
        DB.update("landings", id, map);
    }

    //Стартовая загрузка лидов лендинга
    public void loadLeads() {
        if (widgets.size() == 0)//Если выполняется первый раз size=0
            leads = Lead.getLeads(this, name);//Пробуем получить лидов из БД
        if (leads.size() == 0)//При отсутствии загруженных
            loadLeads(0);//Получаем с сайта
    }

    //Загрузка всех лидов начиная с last_id
    public void loadLeads(int last_id) {
        //Заполняем массив POST-запроса
        HashMap<String, String> data = new HashMap<>();
        data.put("what", "land_leads");//Что хотим получить
        data.put("since", last_id + "");//С какого ID
        data.put("name", name);//Какой лендинг
        String result = Core.get(site.host, site.token, data);//Посылаем запрос на сайт
        //При ошибке завершаем работу
        if (result == null)
            return;
        if (last_id == 0)//Если загружаем заново (полностью), обнуляем старые данные в БД
            DB.delete("landing_leads", "site_id=" + site.id + " AND landing_name='" + name + "'", null);
        try {
            //Распарсиваем полученную JSON-строку
            JSONObject node_root = new JSONObject(result);
            Iterator<String> keys = node_root.keys();
            if (keys == null)
                return;
            //Пока есть записи
            while (keys.hasNext()) {
                JSONObject lead = node_root.getJSONObject(keys.next());
                //Заполняем ассоциативный массив
                HashMap<String, String> map = new HashMap<>();
                map.put("original_id", lead.getInt("id") + "");
                map.put("site_id", site.id + "");
                map.put("landing_name", name);
                map.put("name", lead.getString("name"));
                map.put("phone", lead.getString("phone"));
                map.put("email", lead.getString("email"));
                map.put("status", lead.getInt("status") + "");

                Lead.addLead(map);//Добавляем в БД
                last_lead_id = Math.max(last_lead_id, lead.getInt("id"));//Запоминаем последний ID
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        leads = Lead.getLeads(this, name);//Получаем лидов из БД
        //Сохраняем последний ID
        HashMap<String, String> map = new HashMap<>();
        map.put("last_lead_id", last_lead_id + "");
        DB.update("landings", id, map);
    }

    //Стартовая загрузка статистики лендинга
    public void loadStatistics() {
        if (widgets.size() == 0)//Если выполняется первый раз size=0
            statistics = Statistic.getStatistic(this, name);//Пробуем получить статистику из БД
        if (statistics.size() == 0)//При отсутствии загруженной
            loadStatistics(0);//Получаем с сайта
    }

    //Загрузка всей статистики начиная с last_id
    public void loadStatistics(int last_id) {
        //Заполняем массив POST-запроса
        HashMap<String, String> data = new HashMap<>();
        data.put("what", "land_statistics");//Что хотим получить
        data.put("when", "-1");
        data.put("since", last_id + "");//С какого ID
        data.put("name", name);//Какой лендинг
        String result = Core.get(site.host, site.token, data);//Посылаем запрос на сайт
        //При ошибке завершаем работу
        if (result == null)
            return;
        if (last_id == 0)//Если загружаем заново (полностью), обнуляем старые данные в БД
            DB.delete("landing_statistics", "site_id=" + site.id + " AND landing_name='" + name + "'", null);
        try {
            //Распарсиваем полученную JSON-строку
            JSONObject node_root = new JSONObject(result);
            Iterator<String> keys = node_root.keys();
            if (keys == null)
                return;
            //Пока есть записи
            while (keys.hasNext()) {
                JSONObject stat = node_root.getJSONObject(keys.next());
                //Заполняем ассоциативный массив
                HashMap<String, String> map = new HashMap<>();
                map.put("site_id", site.id + "");
                map.put("landing_name", name);
                map.put("original_id", stat.getInt("id") + "");
                map.put("day", stat.getString("day"));
                map.put("leads", stat.getInt("leads") + "");
                map.put("views", stat.getInt("views") + "");
                map.put("h0_2", stat.getInt("h0-2") + "");
                map.put("h2_4", stat.getInt("h2-4") + "");
                map.put("h4_6", stat.getInt("h4-6") + "");
                map.put("h6_8", stat.getInt("h6-8") + "");
                map.put("h8_10", stat.getInt("h8-10") + "");
                map.put("h10_12", stat.getInt("h10-12") + "");
                map.put("h12_14", stat.getInt("h12-14") + "");
                map.put("h14_16", stat.getInt("h14-16") + "");
                map.put("h16_18", stat.getInt("h16-18") + "");
                map.put("h18_20", stat.getInt("h18-20") + "");
                map.put("h20_22", stat.getInt("h20-22") + "");
                map.put("h22_24", stat.getInt("h22-24") + "");

                Statistic.addStatistic(map);//Добавляем в БД
                last_statistics_id = Math.max(last_statistics_id, stat.getInt("id"));//Запоминаем последний ID
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        statistics = Statistic.getStatistic(this, name);//Получаем статистику из БД
        //Сохраняем последний ID
        HashMap<String, String> map = new HashMap<>();
        map.put("last_statistics_id", last_statistics_id + "");
        DB.update("landings", id, map);
    }

}
