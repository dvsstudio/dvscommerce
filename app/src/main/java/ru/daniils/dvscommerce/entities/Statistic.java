package ru.daniils.dvscommerce.entities;

import android.database.Cursor;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import ru.daniils.dvscommerce.Core;
import ru.daniils.dvscommerce.DB;

/*
Сущность дня статистики
 */
public class Statistic {
    public int id;
    public String day;
    public int leads;
    public int views;
    public int visits;
    public int hours[];
    int original_id;
    Landing landing;

    private Statistic(Cursor c, Landing _landing) {
        load(c);
        landing = _landing;
    }

    //Получение загруженной статистики из БД
    static HashMap<String, Statistic> getStatistic(Landing land, String landing_name) {
        HashMap<String, Statistic> ret = new HashMap<>();
        Cursor c = DB.query("landing_statistics", null, "site_id=" + land.site.id + " AND landing_name='" + landing_name + "'", null, null, null, "original_id DESC");
        if (c == null || !c.moveToFirst()) return ret;
        do {
            ret.put(c.getString(c.getColumnIndex("day")), new Statistic(c, land));
        } while (c.moveToNext());
        c.close();
        return ret;
    }

    //Добавление дня статистики в БД
    static int addStatistic(Map<String, String> day) {
        return (int) DB.insert("landing_statistics", day);
    }

    //Удаление дня статистики из БД по ID
    static boolean removeById(int id) {
        return DB.removeById("landing_statistics", id) == 1;
    }

    //Загрузка основных полей дня статистики из БД по Cursor
    private void load(Cursor c) {
        id = c.getInt(c.getColumnIndex("id"));
        original_id = c.getInt(c.getColumnIndex("original_id"));
        day = c.getString(c.getColumnIndex("day"));
        leads = c.getInt(c.getColumnIndex("leads"));
        views = c.getInt(c.getColumnIndex("views"));
        hours = new int[12];
        visits = hours[0] = c.getInt(c.getColumnIndex("h0_2"));
        visits += hours[1] = c.getInt(c.getColumnIndex("h2_4"));
        visits += hours[2] = c.getInt(c.getColumnIndex("h4_6"));
        visits += hours[3] = c.getInt(c.getColumnIndex("h6_8"));
        visits += hours[4] = c.getInt(c.getColumnIndex("h8_10"));
        visits += hours[5] = c.getInt(c.getColumnIndex("h10_12"));
        visits += hours[6] = c.getInt(c.getColumnIndex("h12_14"));
        visits += hours[7] = c.getInt(c.getColumnIndex("h14_16"));
        visits += hours[8] = c.getInt(c.getColumnIndex("h16_18"));
        visits += hours[9] = c.getInt(c.getColumnIndex("h18_20"));
        visits += hours[10] = c.getInt(c.getColumnIndex("h20_22"));
        visits += hours[11] = c.getInt(c.getColumnIndex("h22_24"));

    }

    //Перезагрузка(Обновление) дня статистики
    public void reload() {
        HashMap<String, String> data = new HashMap<>();
        data.put("what", "land_statistics");
        data.put("when", day);
        data.put("name", landing.name);
        String result = Core.get(landing.site.host, landing.site.token, data);
        if (result == null)
            return;
        try {
            JSONObject land = new JSONObject(result);
            HashMap<String, String> map = new HashMap<>();
            map.put("day", land.getString("day"));
            map.put("leads", land.getInt("leads") + "");
            map.put("views", land.getInt("views") + "");
            map.put("h0_2", land.getInt("h0-2") + "");
            map.put("h2_4", land.getInt("h2-4") + "");
            map.put("h4_6", land.getInt("h4-6") + "");
            map.put("h6_8", land.getInt("h6-8") + "");
            map.put("h8_10", land.getInt("h8-10") + "");
            map.put("h10_12", land.getInt("h10-12") + "");
            map.put("h12_14", land.getInt("h12-14") + "");
            map.put("h14_16", land.getInt("h14-16") + "");
            map.put("h16_18", land.getInt("h16-18") + "");
            map.put("h18_20", land.getInt("h18-20") + "");
            map.put("h20_22", land.getInt("h20-22") + "");
            map.put("h22_24", land.getInt("h22-24") + "");

            DB.update("landing_statistics", id, map);
            Cursor c = DB.query("landing_statistics", null, "id=" + id, null, null, null, null);
            if (c != null && c.moveToFirst())
                load(c);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
