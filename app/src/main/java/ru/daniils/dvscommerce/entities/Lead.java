package ru.daniils.dvscommerce.entities;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import ru.daniils.dvscommerce.Core;
import ru.daniils.dvscommerce.DB;
import ru.daniils.dvscommerce.Utils;

/*
Сущность лида
 */
public class Lead {
    public int id;
    public String name;
    public String phone;
    public String email;
    public int status;
    private int original_id;
    private Landing landing;

    private Lead(Cursor c, Landing _landing) {
        load(c);
        landing = _landing;
    }

    //Получение загруженных лидов из БД
    static ArrayList<Lead> getLeads(Landing land, String landing_name) {
        ArrayList<Lead> ret = new ArrayList<>();
        Cursor c = DB.query("landing_leads", null, "site_id=" + land.site.id + " AND landing_name='" + landing_name + "'", null, null, null, "original_id DESC");
        if (c == null || !c.moveToFirst()) return ret;
        do {
            ret.add(new Lead(c, land));
        } while (c.moveToNext());
        c.close();
        return ret;
    }

    //Добавление лида в БД
    static int addLead(Map<String, String> lead) {
        return (int) DB.insert("landing_leads", lead);
    }

    //Удаление лида из БД по ID
    static boolean removeById(int id) {
        return DB.removeById("landing_leads", id) == 1;
    }

    //Загрузка основных полей лида из БД по Cursor
    private void load(Cursor c) {
        id = c.getInt(c.getColumnIndex("id"));
        original_id = c.getInt(c.getColumnIndex("original_id"));
        name = c.getString(c.getColumnIndex("name"));
        phone = c.getString(c.getColumnIndex("phone"));
        email = c.getString(c.getColumnIndex("email"));
        status = c.getInt(c.getColumnIndex("status"));
    }

    //Звонок лиду
    public void call() {
        //Проверяем права на звонки
        if (!Utils.checkPermission(android.Manifest.permission.CALL_PHONE))
            return;
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));
        Core.getInstance().context.startActivity(intent);
        status = 3;
        HashMap<String, String> map = new HashMap<>();
        map.put("status", "3");
        DB.update("landing_leads", id, map);
    }

    //Принятие лида
    public String apply() {
        //Заполняем массив POST-запроса
        HashMap<String, String> data = new HashMap<>();
        data.put("what", "lead_status");
        data.put("original_id", String.format("%d", original_id));
        data.put("status", "1");
        String result = Core.set(landing.site.host, landing.site.token, data);//Посылаем запрос на сайт
        if (result != null && result.equals("OK")) {
            status = 1;
            HashMap<String, String> map = new HashMap<>();
            map.put("status", "1");
            DB.update("landing_leads", id, map);
        }
        return result;
    }

    //Отклонение
    public String refuse() {
        //Заполняем массив POST-запроса
        HashMap<String, String> data = new HashMap<>();
        data.put("what", "lead_status");
        data.put("original_id", String.format("%d", original_id));
        data.put("status", "2");
        String result = Core.set(landing.site.host, landing.site.token, data);//Посылаем запрос на сайт
        if (result != null && result.equals("OK")) {
            status = 2;
            HashMap<String, String> map = new HashMap<>();
            map.put("status", "2");
            DB.update("landing_leads", id, map);
        }
        return result;
    }
}
