package ru.daniils.dvscommerce.entities;

import android.database.Cursor;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import ru.daniils.dvscommerce.Core;
import ru.daniils.dvscommerce.DB;

/*
Сущность сайта.
Получение данных о лендингах
 */
public class Site {
    public int id;
    public String host;
    public String token;
    public String email;
    public String password;
    public ArrayList<Landing> landings;
    public boolean is_visible;
    public int group_id;

    public Site(int _id) {
        id = _id;
        Cursor c = DB.query("sites", null, "id=" + id, null, null, null, null);
        if (!(c != null && c.moveToFirst()))
            return;
        landings = new ArrayList<>();
        load(c);
        c.close();
    }

    private Site(Cursor c) {
        landings = new ArrayList<>();
        load(c);
    }

    //Получение загруженных сайтов из БД
    public static ArrayList<Site> getSites() {
        ArrayList<Site> ret = new ArrayList<>();

        Cursor c = DB.query("sites", null, null, null, null, null, "id DESC");
        if (c == null || !c.moveToFirst()) return ret;
        do {
            ret.add(new Site(c));
        } while (c.moveToNext());
        c.close();
        return ret;
    }

    //Добавление сайта в БД
    public static int addSite(String host, String email, String password, String token) {
        Map<String, String> site = new HashMap<>();
        site.put("host", host);
        site.put("token", token);
        site.put("email", email);
        site.put("password", password);
        return (int) DB.insert("sites", site);
    }

    //Удаление сайта из БД по ID
    public static boolean removeById(int id) {
        return DB.removeById("sites", id) == 1;
    }

    //Загрузка основных полей сайта из БД по Cursor
    private void load(Cursor c) {
        id = c.getInt(c.getColumnIndex("id"));
        is_visible = true;
        host = c.getString(c.getColumnIndex("host"));
        token = c.getString(c.getColumnIndex("token"));
        email = c.getString(c.getColumnIndex("email"));
        password = c.getString(c.getColumnIndex("password"));
        loadLandings();
    }

    //Стартовая загрузка лендингов
    private void loadLandings() {
        if (landings.size() == 0)//Если выполняется первый раз size=0
            landings = Landing.getLandings(this);//Пробуем получить лендинги из БД
        if (landings.size() == 0)//При отсутствии загруженных
            reloadLandings();//Получаем с сайта
    }

    //Загрузка всех лендингов с сайта
    public void reloadLandings() {
        //Заполняем массив POST-запроса
        HashMap<String, String> data = new HashMap<>();
        data.put("what", "landings");//Что хотим получить
        String result = Core.get(host, token, data);//Посылаем запрос на сайт
        //При ошибке завершаем работу
        if (result == null)
            return;
        DB.delete("landings", "site_id=" + id, null);//Обнуляем старые данные в БД
        is_visible = true;
        try {
            //Распарсиваем полученную JSON-строку
            JSONObject node_root = new JSONObject(result);
            Iterator<String> keys = node_root.keys();
            if (keys == null)
                return;
            //Пока есть записи
            while (keys.hasNext()) {
                JSONObject land = node_root.getJSONObject(keys.next());
                //Заполняем ассоциативный массив
                HashMap<String, String> map = new HashMap<>();
                map.put("name", land.getString("name"));
                map.put("title", land.getString("title"));
                map.put("options", land.getString("options"));
                map.put("last_widget_id", "0");
                map.put("last_lead_id", "0");
                map.put("last_statistics_id", "0");
                map.put("positions", land.getString("positions"));
                map.put("site_id", id + "");
                map.put("published", land.getInt("published") + "");

                Landing.addLanding(map);//Добавляем в БД
                //Загружаем фавикон лендинга
                Core.loadFile(host, "/templates/" + land.getString("name") + "/images/favicon.ico", "/favicons/f" + id + "-" + land.getString("name") + ".ico");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        landings = Landing.getLandings(this);//Получаем лендинги из БД
    }
}
